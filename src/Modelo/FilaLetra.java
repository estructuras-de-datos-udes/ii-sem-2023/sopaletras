/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Lab05pc05
 */
public class FilaLetra {

    private Letra filas[];

    public Letra[] getFilas() {
        return filas;
    }

    public void setFilas(Letra[] filas) {
        this.filas = filas;
    }

    public FilaLetra() {
    }

    public FilaLetra(Letra[] filas) {
        this.filas = filas;
    }

    public FilaLetra(String letras) {
        String respuesta[] = letras.split(",");
        int tamaño = respuesta.length;
        this.filas = new Letra[tamaño];
        for (int i = 0; i < tamaño; i++) {
            String dato = respuesta[i];
            char letra = dato.charAt(0);
            this.filas[i] = new Letra(letra);

        }

    }

    @Override
    public String toString() {
        String mensaje = "";
        for (Letra dato : this.filas) {
            mensaje += dato.getLetra() + "\t";

        }
        return mensaje;
    }

    public boolean isLetra(char letra) {
        for (Letra dato : this.filas) {
            char x = dato.getLetra();
            if (x == letra || x == Character.toLowerCase(letra)) {
                return true;
            }
        }
        return false;

    }

    private int getValor(char letra) {
        return (int) (letra - 97);
    }
    
    private char getLetra(int letra)
    {
        return (char)(letra+97);
    }

    public String getCantidadLetras() {
        String resultado = "";
        int cubetas[] = new int[25];
        for (Letra myLetra : this.filas) {
            char unaLetra = myLetra.getLetra();
            //esto sirve para mayúsculas
            //char unaLetra=Character.toLowerCase(myLetra.getLetra());
            int pos = this.getValor(unaLetra);
            cubetas[pos]++;
        }
        int i=0;
        for (int dato : cubetas) {
            resultado += this.getLetra(i)+"="+dato + "\t";
            i++;
        }
        return resultado;
    }
    
    
    public String getLetrasMasSeRepiten()
    {
        return "";
    }
    
}
